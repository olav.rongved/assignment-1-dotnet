﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FileSystemManager.Logger;

namespace FileSystemManager.FileManipulation
{
    // TextFileService which reads text
    public class TextFileService
    {

        // read textfile line by line
        public static void ReadTextFile(string path)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        // print file info for linecount and size in bytes
        public static void PrintFileInfo(string path)
        {
            // time for logging purposes
            var watch = System.Diagnostics.Stopwatch.StartNew();

            // Get lineCount and bytes from other methods
            int lineCount = LineCounter(path);
            long fileSize = FileSizeInfo(path);

            // Generate the output message
            string outMessage = string.Format("\nName: dracula.txt" +
                                "\n\nSize in bytes: {0}" +
                                "\n Number of lines: {1}", 
                                lineCount, fileSize);


            watch.Stop();
            DateTime currentTime = DateTime.Now;

            // Logging purposes
            string logMessage = $"{currentTime}: {outMessage} : Execution took {watch.ElapsedMilliseconds} ms";
            Console.WriteLine(logMessage);
            Logger.Logger.Log(logMessage);

            Console.WriteLine(outMessage);
        }

        // Method to get filesize
        public static long FileSizeInfo(string path)
        {

            try
            {
                FileInfo fi = new FileInfo(path);

                // return length of file, which is returned in the form of bytes in long format
                return fi.Length;

            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            return 0;

        }

        // method to count lines in file
        public static int LineCounter(string path)
        {
            // counter
            int lineCount = 0;
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string line;
                    // check if next line if null, if not null increment linecount
                    while ((line = sr.ReadLine()) != null)
                    {
                        lineCount++;
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return lineCount;

        }

        // search method
        public static void SearchTextFile(string path, string search)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            
            bool containsSearch = false;
            int searchOccurrences = 0;
            string outMessage = "";

            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    // For each line, convert to lowercase, and search based on user input
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        searchOccurrences += LazyLineSearch(line.ToLower(), search.ToLower());
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (searchOccurrences > 0) { containsSearch = true; }


            // Different messages depending on if searchword was found or not
            if (containsSearch)
            {
                outMessage = string.Format("Dracula contains {0} occurrences of {1}", searchOccurrences, search);
                Console.WriteLine("Dracula contains {0} occurrences of {1}", searchOccurrences, search);
            } else
            {
                outMessage = string.Format("Dracula.txt does not contain {0}", search);
                Console.WriteLine("Dracula.txt does not contain {0}", search);
            }

            watch.Stop();
            DateTime currentTime = DateTime.Now;

            // Logging purposes
            string logMessage = $"{currentTime}: {outMessage} : Execution took {watch.ElapsedMilliseconds} ms";
            Console.WriteLine(logMessage);
            Logger.Logger.Log(logMessage);
        }


        // Lazy line search
        public static int LazyLineSearch(string line, string search)
        {
            // Simple trick that splits based on search word, which should then return number of hits + 1
            if (line.Contains(search))
            {
                string[] splitArray = line.Split(search);
                return splitArray.Length - 1;
            }



            
            
            return 0;

            
        }

        public static void PrintTextFileInfo(string path)
        {
            string name;
            string size;
            uint numberOfLines;

        }

    }

}

