﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace FileSystemManager.FileManipulation
{

    // Class that holds the responsability to read files
    public class FileReader
    {

        

        // Path to resources folder
        public static string root = @"..\..\..\resources";


        // Method that prints folders 
        public static void PrintFolders(string[] filenames)
        {
            // Time the inner works
            var watch = System.Diagnostics.Stopwatch.StartNew();


            // print each filename present in filenames string array
            foreach (var item in filenames)
            {
                Console.WriteLine(item);
            }

            DateTime currentTime = DateTime.Now;
            watch.Stop();


            // Manage message for logging
            string outMessage = "Printing folder structure..";
            string logMessage = $"{currentTime}: {outMessage} : Execution took {watch.ElapsedMilliseconds} ms";
            Console.WriteLine(logMessage);
            Logger.Logger.Log(logMessage);
        }

        // Method that prints filenames by extension
        public static void PrintFolderExtensions(string[] filenames, string extension)
        {
            // Time for logging purposes
            var watch = System.Diagnostics.Stopwatch.StartNew();


            // Iterate over filenames present
            foreach (string item in filenames)
            {
                // Check if filename contains the desired extension, if yes print.
                if (item.Contains(extension))
                {
                    string[] fileExtension = item.Split(@".");
                    Console.WriteLine(item);
                }
            }

            DateTime currentTime = DateTime.Now;
            watch.Stop();

            // Logging purposes
            string outMessage = $"Printing files based on {extension}";
            string logMessage = $"{currentTime}: {outMessage} : Execution took {watch.ElapsedMilliseconds} ms";
            Console.WriteLine(logMessage);
            Logger.Logger.Log(logMessage);
        }



    }
}
