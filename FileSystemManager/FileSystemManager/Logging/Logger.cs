﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSystemManager.Logger
{
    // Logger class that writes to log
    public class Logger
    {
        public static void Log(string message)
        {
            Console.WriteLine("Writing to log...");
            try
            {

                //using FileStream fs = File.Create(@"..\..\..\resources\Log.txt");

                // open Log.txt and append to file
                using (StreamWriter sw = new StreamWriter(@"..\..\..\resources\Log.txt",true))
                {
                    sw.Write(message+ "\n");
                }
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
