﻿using System;
using System.Collections.Generic;
using System.Text;
using FileSystemManager.FileManipulation;
namespace FileSystemManager.UI
{
    public class User
    {
        // change to mainloop

        // UI Loop
        public static void UserLoop()
        {
            // Main menu options
            string MenuString = "\n\nWelcome to the best filemanager system!\n" +
                                "These are the available options: \n\n" +
                                "1) \t List all filenames in the resource directory\n" +
                                "2) \t List specific files based on file extension\n" +
                                "3) \t Search Dracula.txt\n" +
                                "4) \t Display info on Dracula.txt\n\n" +
                                "5) \t Exit program\n";


            // message for cases based on user input
            string extensionMessage = "Please enter your desired extension(without .):";
            string searchMessage = "Please enter desired searchword:";

            // bool that controls if program stays on menu or exits
            bool runProgram = true;
            while(runProgram)
            {
                Console.WriteLine(MenuString);
                int selectedItem;

                // Get user input and convert to int
                ConsoleKeyInfo input = Console.ReadKey();
                int.TryParse(input.KeyChar.ToString(), out selectedItem);
                string[] files = System.IO.Directory.GetFiles(@"..\..\..\resources");


                // Menu for user, cases controls what happens
                switch (selectedItem)
                {
                    case 1:
                        
                        Console.WriteLine(selectedItem.ToString());
                        FileReader.PrintFolders(files);
                        break;
                    case 2:
                        Console.WriteLine("\n" + extensionMessage + "\n");
                        string extensionInput = Console.ReadLine();
                        FileReader.PrintFolderExtensions(files, extensionInput);

                        break;
                    case 3:
                        Console.WriteLine("\n" + searchMessage + "\n");
                        string searchInput = Console.ReadLine();
                        TextFileService.SearchTextFile(@"..\..\..\resources\dracula.txt", searchInput);
                        break;

                    case 4:
                        TextFileService.PrintFileInfo(@"..\..\..\resources\dracula.txt");
                        break;
                    case 5:
                        runProgram = false;
                        break;
                    default:
                        break;
                }

            }


        }
    }
}
