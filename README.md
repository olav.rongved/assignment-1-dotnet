Description:

A simple console application that can display info on a .txt file, list files in a folder, and search for number of occurrences of words within a .txt file. This is logged in a file '/resources/Log.txt", which holds timestamp information, information on the output, and execution time in ms.

Usage:

To use, first run program.cs, should create a prompt with several options.

Press a key based on the options seen in the menu in order to use the desired function.
